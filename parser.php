<?php

require_once('api/Api.php');
require_once('include/phpQuery/phpQuery/phpQuery.php');

class Parser extends Api
{
    const MAX_COUNT_TRACKS = 5;
    const IS_PARSE  = 1;
    const NOT_PARSE = 0;

    private $_curl_class;
    private $_base_url = 'https://www.youtube.com/results?search_query=';

    public function __construct()
    {
        $this->_curl_class = get_class($this->curl);
    }

    public function process()
    {
        $performers = $this->performer->get_performers(array(
            'is_parse' => self::NOT_PARSE,
            ));

        if (count($performers)) {

            foreach ($performers as $performer) {

                /**@var Curl $curl*/
                $curl = new $this->_curl_class;
                $curl->setUrl($this->_base_url . urlencode($performer->title));
                $content = $curl->content();

                if($this->_add_info($performer->id, $content)) {

                    $this->performer->update_performer($performer->id, array(
                        'is_parse' => self::IS_PARSE,
                    ));
                }

                sleep(rand(2, 6));
            }
        }

        echo "DONE\n";

        return true;
    }

    private function _add_info($performer_id, $content) {

        $doc = phpQuery::newDocument($content);

        // Записи
        $hentry = $doc->find('div.yt-lockup-video');

        if (empty($hentry)) {

            return false;
        }

        $counter = 0;
        foreach ($hentry as $el) {

            $pq = pq($el);

            $title = $pq->find('h3.yt-lockup-title a')->text();
            $count_views = preg_replace('/\D/', '', $pq->find('ul.yt-lockup-meta-info li:last')->text());
            $url   = $pq->find('h3.yt-lockup-title a')->attr('href');

            if (empty($count_views) || empty($url) || empty($title)) {

                continue;
            }

            $this->track->create_track(array(
                'performer_id' => $performer_id,
                'title'        => $title,
                'count_views'  => $count_views,
                'url'          => $url,
            ));

            if (self::MAX_COUNT_TRACKS == ++$counter) {

                break;
            }
        }

        return true;
    }
}

try {

    $page = new Parser();
    $page->process();
} catch (Exception $e) {

    echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
}
