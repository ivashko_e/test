<?php

require_once('Api.php');

class View extends Api
{
    private $_root_path;

    public function  __construct()
    {
        $this->_root_path = $_SERVER['DOCUMENT_ROOT'];
    }

    public function render($template)
    {
        if (! is_file($filename = $this->_root_path . '/template/' . $template . '.php')) {

            return;
        }

        ob_start();
        include_once $filename;
        $content = ob_get_contents();
        ob_end_clean();

        return $content;
    }

    public function partial($template, $params = array())
    {
        $view = new View();

        foreach ($params as $k => $v) {

            $view->{$k} = $v;
        }

        return $view->render($template);
    }

    public function __set($name, $value)
    {
        $this->{$name} = $value;
    }

    public function __get($name)
    {
        if (! empty($this->{$name})) {
            return $this->{$name};
        }

        return;
    }
}
