<?php

class Curl
{
    private $_ch;
    private $_url;
    private $_chOptions = [
        CURLOPT_USERAGENT      => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.65 Safari/537.36',
        CURLOPT_HEADER         => FALSE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_SSL_VERIFYPEER => FALSE,
        CURLOPT_SSL_VERIFYHOST => 2,
        CURLOPT_FOLLOWLOCATION => TRUE,
        CURLOPT_AUTOREFERER    => TRUE,
        CURLINFO_HEADER_OUT    => TRUE
    ];

    public function __construct($options = [])
    {
        $this->_ch = curl_init();

        curl_setopt_array($this->_ch, $this->_chOptions);
    }

    public function __destruct()
    {
        $this->close();
    }

    public function getCh()
    {
        return $this->_ch;
    }

    public function setOptions($options = [])
    {
        if (!empty($options)) {

            foreach ($options as $k => $v) {

                $this->setOption($k, $v);
            }
        }

        return $this;
    }

    public function setOption($key, $value)
    {
        if ($this->_ch) {
            curl_setopt($this->_ch, $key, $value);
        }
        return $this;
    }

    public function setUrl($url)
    {
        $this->setOption(CURLOPT_URL, $url);
        $this->_url = $url;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        $info = curl_getinfo($this->_ch);

        if (!empty($info['url'])) {

            return $info['url'];
        }

        return $this->_url;
    }

    public function setReferer($referer)
    {
        $this->setOption(CURLOPT_REFERER, $referer);

        return $this;
    }

    public function setProxy($proxy)
    {
        if (empty($proxy)) {

            throw new Exception('Не удалось получить адрес прокси');
        }

        $this->setOptions([
            CURLOPT_PROXY     => $proxy->ip,
            CURLOPT_PROXYPORT => $proxy->port,
        ]);

        if ($proxy->login !== '' && $proxy->password !== '') {

            $this->setOption(CURLOPT_PROXYUSERPWD, $proxy->login . ':' . $proxy->password);
        }

        return $this;
    }

    public function setCookieFile($cookieFile)
    {
        $this->setOptions([
            CURLOPT_COOKIEFILE => $cookieFile,
            CURLOPT_COOKIEJAR  => $cookieFile,
        ]);

        return $this;
    }

    public function setPost($params = NULL)
    {
        if (empty($params)) {

            $this->setOptions([
                CURLOPT_POST => FALSE,
            ]);

            return $this;
        }

        $this->setOptions([
            CURLOPT_POST          => TRUE,
            CURLOPT_POSTFIELDS    => $params,
        ]);

        return $this;
    }

    public function setUserAgent($userAgent = NULL)
    {
        if (empty($userAgent)) {

            $this->setOption(CURLOPT_USERAGENT, '');

            return $this;
        }

        $this->setOption(CURLOPT_USERAGENT, $userAgent);

        return $this;
    }

    public function content()
    {
        $content = curl_exec($this->_ch);

        return $content;
    }

    public function info()
    {
        $info = curl_getinfo($this->_ch);

        return $info;
    }

    public function error()
    {
        $error = curl_error($this->_ch);

        return $error;
    }

    public function close()
    {
        if ($this->_ch) {
            @curl_close($this->_ch);
        }

        return $this;
    }
}