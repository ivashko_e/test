<?php

require_once('Api.php');

class Performer extends Api
{
	// Возвращает список исполнителей
	public function get_performers($filter = array())
	{
		$status = "enabled";

		if (isset($filter['status'])) {

			$status = $filter['status'];
		}

		$is_parse_filter = '';
		if (isset($filter['is_parse'])) {

			$is_parse_filter = $this->db->placehold("AND p.is_parse=?", $filter['is_parse']);
		}

		$query = $this->db->placehold("SELECT
										p.id,
										p.title,
										p.status,
										SUM(t.count_views) AS sum_count_views,
										(SELECT url FROM track WHERE performer_id = p.id ORDER BY count_views DESC LIMIT 1) AS url
										FROM performer p
										LEFT JOIN track t ON p.id = t.performer_id
										WHERE p.status=? $is_parse_filter GROUP BY p.id", $status);

		if($this->db->query($query)) {

            return $this->db->results();
        } else {

            return false;
        }
	}

	// Редактирование исполнителя
	public function update_performer($id, $data)
	{
		$query = $this->db->placehold("UPDATE performer SET ?% WHERE id=?", $data, $id);
		$this->db->query($query);
		return true;
	}
}
