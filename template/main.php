<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo $this->tag_title; ?></title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/sb-admin.css" rel="stylesheet">
    <script src="js/jquery-1.10.2.js"></script>
    <link href="//cdn.datatables.net/1.10.0/css/jquery.dataTables.css" rel="stylesheet">
</head>

<body>

<?php echo $this->content; ?>

<div class="clearfix"></div>

<script src="js/bootstrap.min.js"></script>
<script src="js/sb-admin.js"></script>
<script src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>

</body>

</html>
