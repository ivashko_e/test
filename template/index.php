<script>
    $(function () {
        $('#performers').dataTable({
            'language' : 'ru'
        });
    });
</script>


<div id="wrapper">

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><?= $this->h1 ?></h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">

            <div class="panel panel-default">
                <div class="panel-heading">
                    Исполнители
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper">

                            <table id="performers" class="table table-striped table-hover">
                                <thead>
                                <tr role="row">
                                    <th style="width: 10px;">
                                        #
                                    </th>
                                    <th>
                                        Имя исполнителя
                                    </th>
                                    <th style="width: 350px;">
                                        Самый популярный ролик
                                    </th>
                                    <th style="width: 200px;">
                                        Количество просмотров
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if(!empty($this->performers)) : ?>
                                    <?php foreach ($this->performers as $performer) : ?>
                                            <tr>
                                                <td class="center"><?= $performer->id ?></td>
                                                <td><?= $performer->title ?></td>
                                                <td>
                                                    <?php if(!empty($performer->url)): ?>
                                                        <a target="_blank" href="https://www.youtube.com<?= $performer->url ?>">
                                                            https://www.youtube.com<?= $performer->url ?></a>
                                                    <?php endif; ?>
                                                </td>
                                                <td class="center">
                                                    <?= $performer->sum_count_views ?>
                                                </td>
                                            </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
                <!-- /.table-responsive -->
            </div>
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->


</div>
<!-- /#wrapper -->
