<?php

require_once('api/Api.php');

class Index extends Api
{
    public function __construct()
    {
        $action = $this->request->get('action');

        if (! empty($action)) {

        } else {
            $action = 'index';
        }

        $this->{$action . "Action"}();
    }

    public function indexAction()
    {
        $this->view->tag_title = 'Исполнители';
        $h1                    = 'Исполнители';

        $performers = $this->performer->get_performers();

        $this->view->content = $this->view->partial('index', array(
            'performers'   => $performers,
            'h1'           => $h1,
        ));

        echo $this->view->render('main');
    }
}

try {

    $page = new Index();
} catch (Exception $e) {

    echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
}
