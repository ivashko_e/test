-- Adminer 4.2.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `performer`;
CREATE TABLE `performer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `is_parse` tinyint(1) NOT NULL DEFAULT '0',
  `status` enum('enabled','disabled') NOT NULL DEFAULT 'enabled',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `performer` (`id`, `title`, `is_parse`, `status`) VALUES
(1,	'Michael Jackson',	0,	'enabled'),
(2,	'Adriano Celentano',	0,	'enabled'),
(3,	'Whitney Houston',	0,	'enabled'),
(4,	'Mireille Mathieu',	0,	'enabled'),
(5,	'Charles Aznavour',	0,	'enabled'),
(6,	'Paul McCartney',	0,	'enabled'),
(7,	'Tina Turner',	0,	'enabled'),
(8,	'Alla Pugacheva',	0,	'enabled'),
(9,	'Madonna',	0,	'enabled'),
(10,	'Elton John',	0,	'enabled'),
(11,	'Joe Cocker',	0,	'enabled'),
(12,	'Stevie Wonder',	0,	'enabled'),
(13,	'Aretha Franklin',	0,	'enabled'),
(14,	'Ray Charles',	0,	'enabled'),
(15,	'Diana Ross',	0,	'enabled'),
(16,	'Steven Tyler',	0,	'enabled'),
(17,	'Elvis Presley',	0,	'enabled'),
(18,	'Freddie Mercury',	0,	'enabled'),
(19,	'David Bowie',	0,	'enabled'),
(20,	'Mick Jagger',	0,	'enabled'),
(21,	'Scorpions',	0,	'enabled'),
(22,	'James Brown',	0,	'enabled'),
(23,	'Lionel Richie',	0,	'enabled'),
(24,	'Barry White',	0,	'enabled'),
(25,	'Ozzy Osbourne',	0,	'enabled'),
(26,	'Louis Armstrong',	0,	'enabled'),
(27,	'ABBA',	0,	'enabled'),
(28,	'Frank Sinatra',	0,	'enabled'),
(29,	'Chris Rea',	0,	'enabled'),
(30,	'Tom Jones',	0,	'enabled'),
(31,	'Luciano Pavarotti',	0,	'enabled'),
(32,	'Andy Williams',	0,	'enabled'),
(33,	'Joe Dassin',	0,	'enabled'),
(34,	'Demis Roussos',	0,	'enabled');

DROP TABLE IF EXISTS `track`;
CREATE TABLE `track` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `performer_id` int(11) unsigned NOT NULL,
  `title` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `count_views` int(11) NOT NULL,
  `status` enum('enabled','disabled') NOT NULL DEFAULT 'enabled',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 2015-09-24 21:51:21
